<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cms' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RaN2)>_21vq.g;s=ND1tHMTuYwk!n@(1hO[; 8?P RtczK|]Q_9Q7fj*6DJgbz*|' );
define( 'SECURE_AUTH_KEY',  '*4aI*_P,>)up!)PYjzf+i#1o,9^LGNH[{K3pP&N:(B-v[[>,|,IC-$d2rC6o=/i%' );
define( 'LOGGED_IN_KEY',    'p(VzAlGx8)aUGrED)]hCB-U90-c(W.<pW2Y #KuZooF*ViV0LM6$4isVe@p3FJaj' );
define( 'NONCE_KEY',        '[bJw+a*||&$Cm:AE)6[H^L58T)45cU-8Npy[`rX4hm)hY%{RCgbA,`%}.TU&^R[h' );
define( 'AUTH_SALT',        '&+shzce A&oa+(HbV4XW)}N9^Mwns&FFRaY@RL~0=7pZ$8GX`C^W#r=&?UVst}g8' );
define( 'SECURE_AUTH_SALT', 'D&~S[Og(Z8)@XZ!}^KEgu+onVyxLxe!%Jt@;*+1v*<;2&9DFi,VD:mA(5rELU!@o' );
define( 'LOGGED_IN_SALT',   'OG~&b5` WACKEn:]{v7lxH[ODkV}u6ZH(K>hH_saA!}!3Ku?tQ#[yR>D4=gXp^44' );
define( 'NONCE_SALT',       ' &)k=x13GL&+U?4UavC=oqF4y~ZGj<`@U2?-N@JqO@F>Cvzw`KU*^w:l]^,sBE6H' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
